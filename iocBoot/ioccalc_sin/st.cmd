#!../../bin/linux-x86_64/calc_sin

#- You may have to change calc_sin to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/calc_sin.dbd"
calc_sin_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/calc_sin.db","user=root")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=root"
